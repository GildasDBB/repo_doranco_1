DROP DATABASE IF EXISTS bdd;

CREATE DATABASE bdd;

USE bdd;

#table trajets fait par Julie et José
CREATE TABLE trajets (
  trajet_id INT AUTO_INCREMENT PRIMARY KEY,
  depart VARCHAR(255) NOT NULL,
  arrivee VARCHAR(255) NOT NULL,
  duree TIME NOT NULL
);

INSERT INTO trajets (depart, arrivee, duree) 
VALUES
('031 Sutteridge Place', '5897 Maryland Drive', '2020/09/12'),
('457 Lotheville Trail', '0 Clyde Gallagher Drive', '2020/09/12'),
('4 Ronald Regan Pass', '40279 Autumn Leaf Center', '2020/09/12'),
('19931 Dennis Lane', '1429 Vahlen Plaza', '2020/09/12'),
('60253 Merchant Pass', '259 Maywood Place', '2020/09/12'),
('4282 Rieder Lane', '068 Carpenter Street', '2020/09/12'),
('89 Eliot Court', '944 Forest Circle', '2020/09/12'),
('4425 Hollow Ridge Street', '3 West Junction', '2020/09/12'),
('907 Esch Place', '4088 Sachtjen Way', '2020/09/12'),
('27175 Mccormick Terrace', '800 Northwestern Lane', '2020/09/12');

#table colors fait par Tai et Mainmouna

CREATE TABLE IF NOT EXISTS colors
(
    color_id INT AUTO_INCREMENT PRIMARY KEY,
    name varchar (30) NOT NULL,
    code_hexa varchar (30) NOT NULL,
    specifications varchar (30) NOT NULL
);

INSERT INTO colors (name, code_hexa, specifications)
VALUES
('Turquoise', '#53c0ee', 'dark color'),
('Green', '#ff2553', 'light color'),
('Goldenrod', '#73f3c3', 'light color'),
('Puce', '#33a390', 'dark color'),
('Puce', '#27512a', 'light color'),
('Mauv', '#d93787', 'dark color'),
('Mauv', '#3e70ae', 'light color'),
('Maroon', '#dabeaf', 'dark color'),
('Violet', '#fc052c', 'light color'),
('Purple', '#ee3e67', 'light color');

#table voitures fait par Naima et Ibrahim
CREATE TABLE IF NOT EXISTS Voiture (
  `voiture_id` INT AUTO_INCREMENT PRIMARY KEY,
  `color_id`INT,
  `trajet_id` INT,
  `marque` VARCHAR(25) NOT NULL,
  `modele` VARCHAR(25) NOT NULL,
  FOREIGN KEY (trajet_id)
    REFERENCES trajets(trajet_id),
  FOREIGN KEY (color_id)
    REFERENCES colors (color_id)
);

#table users fait par Bérangère et Nicolas
CREATE TABLE USERS
(
    user_id AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(100),
    firstname VARCHAR(100),
    email VARCHAR(255),
    birth DATE,
    sexe ENUM ('H',  'F'),
    `trajet_id` INT,
    FOREIGN KEY (trajet_id)
      REFERENCES trajets(trajet_id)
);

#7 table documents fait par TRINOME Kleida, Hoda et Patricia
CREATE TABLE `documents` (
  `document_id` INT(11) AUTO_INCREMENT PRIMARY KEY,
  `type` VARCHAR (30) NOT NULL,
  `fichier` TEXT NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `documents` (`type`, `fichier`) 
VALUES
('Passeport', 'Copie de passeport Kleida'),
('CNI', 'Copie de CNI Kleida'),
('Passeport', 'Copie de passeport Hoda'),
('CNI', 'Copie de CNI Hoda'),
('Passeport', 'Copie de passeport Patricia'),
('CNI', 'Copie de CNI Patricia');

#table adresses fait par Emilie et Jin
CREATE TABLE adresses (
  adresses_id AUTO_INCREMENT PRIMARY KEY,
  numero int(10) NOT NULL,
  type_voie VARCHAR(255) NOT NULL,
  nom_voie VARCHAR(255) NOT NULL,
  ville VARCHAR(255) NOT NULL,
  cp int(10) not null 
);

INSERT INTO adresses (numero, type_voie, nom_voie, ville, cp) 
VALUES 
('14685', 'Coleman', '4 Memorial Park', 'Hövsan', 'AZ'),
('1970', 'Eagle Crest', '29731 Sunfield Trail', 'Gagarin', 'UZ'),
('072', 'Hagan', '22799 West Lane', 'Iksan', 'KR'),
('60082', 'Straubel', '9398 Lillian Plaza', 'Lapid', 'IL'),
('1', 'Lakewood', '630 Waubesa Hill', 'Mungging', 'ID'),
('170', 'Kropf', '6 Randy Avenue', 'Kawungsari', 'ID'),
('8', 'Melrose', '11670 Grasskamp Pass', 'Asheville', 'US'),
('0762', 'Bartelt', '6 Mallory Avenue', 'Lhabupu', 'CN'),
('63746', 'School', '34 Weeping Birch Trail', 'Obršani', 'MK'),
('28', 'Clemons', '903 Coleman Place', 'Ning’an', 'CN');












